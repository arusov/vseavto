package org.vseavto.core.api.bl.parser;

import org.vseavto.core.model.Advertisement;

import java.util.List;

/**
 * User: ARusov
 * Date: 17.07.13
 * Time: 23:34
 */
public abstract class AbstractParser implements Parser {
    @Override
    public abstract List<Advertisement> retrieveListAdvertisements();

    @Override
    public abstract List<Advertisement> parseAdvertisements() ;
}
