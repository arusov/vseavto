package org.vseavto.core.api.bl.parser;

import org.vseavto.core.model.Advertisement;

import java.util.List;

/**
 * User: ARusov
 * Date: 17.07.13
 * Time: 23:36
 */
public interface Parser {
    public List<Advertisement>retrieveListAdvertisements();
    public List<Advertisement>parseAdvertisements();
}
