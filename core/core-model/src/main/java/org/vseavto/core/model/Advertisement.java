package org.vseavto.core.model;

import com.google.gson.Gson;

import java.math.BigDecimal;
import java.util.List;

/**
 * User: ARusov
 * Date: 17.07.13
 * Time: 23:01
 */
public class Advertisement {
    private long id;
    private String url;
    private String tittle;
    private String text;
    private Location location;
    private Rubric rubric;
    private List<Characteristic> characteristics;
    private List<Picture> pictures;

    @Override
    public String toString() {
        Gson gson= new Gson();
        return gson.toJson(this);
    }
}
