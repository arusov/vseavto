package org.vseavto.core.model;

import java.util.List;

/**
 * User: ARusov
 * Date: 17.07.13
 * Time: 23:05
 */
public class Characteristic {

    public static final int CHARACTERISTIC_TYPE_COUNT=0;
    public static final int CHARACTERISTIC_TYPE_STRING=1;

    private int id;
    private String name;
    private int type;
    private List<CharacteristicValue>values;
}
